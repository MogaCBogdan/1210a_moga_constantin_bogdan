import abc

class Handler(metaclass=abc.ABCMeta):
	def __init__(self, succesor = None):
		self.succesor = succesor

	def handle(self, request):
		res = self.check_context(request)
		if not res and self.succesor:
			self.succesor.handle(request)

	def check_context(self, request):
		raise NotImplementedError()

class FallbackHandler(Handler):
	def check_context(self, request):
		print("Lantul a fost parcurs fara potrivire.\n")
		return False

class HandlerPython(Handler):
	def check_context(self, request):
		with open(request, 'r') as fisier:
			for lines in fisier:
				for word in lines.split():
					if word in ["def", "__init__", "__name__", "self"]:
						print("Fisierul " + request + " este de tip Python.\n")
						return True
		fisier.close()
		return False

class HandlerKotlin(Handler):
	def check_context(self, request):
		with open(request, 'r') as fisier:
			for lines in fisier:
				for word in lines.split():
					if word in ["fun", "val", "var", "kotlin", "when"]:
						print("Fisierul " + request + " este de tip Kotlin.\n")
						return True
		fisier.close()
		return False



class HandlerBash(Handler):
	def check_context(self, request):
		with open(request, 'r') as fisier:
			for lines in fisier:
				for word in lines.split():
					if word in ["test", "sh","echo", "bin", "expr", "fi"]:
						print("Fisierul " + request + " este de tip Bash.\n")
						return True
		fisier.close()
		return False

class HandlerJava(Handler):
	def check_context(self,request):
		with open(request, 'r') as fisier:
			for lines in fisier:
				for word in lines.split():
					if word in ["System", "static", ".out.", "new", "void"]:
						print("Fisierul " + request + " este de tip Java.\n")
						return True
		fisier.close()
		return False



if __name__ == "__main__":
	h0 = HandlerKotlin()
	h1 = HandlerPython()
	h2 = HandlerBash()
	h3 = HandlerJava(FallbackHandler())
	h0.succesor = h1
	h1.succesor = h2
	h2.succesor = h3
	request = input("Calea fisierului: ")
	h0.handle(request)