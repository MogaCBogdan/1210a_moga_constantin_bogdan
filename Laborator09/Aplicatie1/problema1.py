class File:
    def __init__(self):
        pass

    def read_file_from_in(self):
        self.title = input("Titlu : ")
        self.autor = input("Autor: ")
        self.paragrafe = []
        n = input("Introduceti nr de paragrafe: ")
        for i in range(int(n)):
            self.paragrafe.append(input("Paragraf: "))


class FileFactory:
    def __init__(self):
        pass

    def factory(self, file_type):
        if file_type == "HTMLFile":
            return HTMLFile()
        if file_type == "JSONFile":
            return JSONFile()
        if file_type == "TextFile":
            return TextFile()
        if file_type == "ArticleTextFile":
            return ArticleTextFile()
        if file_type == "BlogTextFile":
            return BlogTextFile()
        return


class HTMLFile(File):
    def __init__(self):
        self.read_file_from_in()

    def print_html(self):
        print("Titlu: " + self.title)
        print("Autor: " + self.autor)
        for par in self.paragrafe:
            print("<p>" + par + "</p>")


class JSONFile(File):
    def __init__(self):
        self.read_file_from_in()

    def print_json(self):
        print("{")
        print("\t\"titlu\": " + "\"" + self.title + "\"" + ",")
        print("\t\"autor\": " + "\"" + self.autor + "\"" + ",")
        print("\t\"paragrafe\": " + "{")
        for par in self.paragrafe:
            print("\t\t\"paragraf\": \"" + par + "\",")
        print("\t}")
        print("}")


class TextFile(File):
    def __init__(self):
        self.Template = input("Template : ")
        self.read_file_from_in()

    def clone(self):
        return self

    def print_text(self):
        raise NotImplementedError


class BlogTextFile(TextFile):
    def __init__(self):
        self.read_file_from_in()

    def print_text(self):
        print(self.title)
        for par in self.paragrafe:
            print(par)
        print("\n Written by ",self.autor)

class ArticleTextFile(TextFile):
     def __init__(self):
         self.read_file_from_in()

     def print_text(self):
        print("\t\t",self.title)
        print("\t\t\t by ",self.autor)
        for par in self.paragrafe:
            print(par)

if __name__ == "__main__":
    a = HTMLFile()
    a.print_html()

    a = JSONFile()
    a.print_json()

    a = ArticleTextFile()
    a.print_text()

    a = BlogTextFile()
    a.print_text()
