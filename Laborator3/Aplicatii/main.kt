class Birth(val year: Int, val Month: Int, val Day: Int){
    override fun toString() : String{
        return "($Day.$Month.$year)"
    }
}
class Contact(val Name: String, val Phone: String, val BirthDate: Birth){
    fun Print() {
        println("Name: $Name, Mobile: $Phone, Date: $BirthDate")
    }
}
fun Cautare(agenda : List<Contact>) {
    var gasit = 0 ;
    println("Dati numele cautat: ")
    val nume = readLine()

    println("Dati nr de telefon: ")
    val nr = readLine()

    for (persoana in agenda) {
        if (persoana.Name == nume) {
            println("Persoana cautata se gaseste in agenta la indexul " + agenda.indexOf(persoana))
            gasit=1;
        }
        if(gasit!=1){
            if (persoana.Phone == nr) {
                println("Persoana cautata se gaseste in agenta la indexul " + agenda.indexOf(persoana))
                gasit=1;
            }
        }
        if(gasit==0)
            println("Persoana nu exista.")

    }
}

fun main(args : Array<String>){
    val agenda = mutableListOf<Contact>()
    agenda.add(Contact("Mihai", "0744321987", Birth(1900, 11, 25)))
    agenda += Contact("George", "0761332100", Birth(2002, 3, 14))
    agenda += Contact("Liviu" , "0231450211", Birth(1999, 7, 30))
    agenda += Contact("Popescu", "0211342787", Birth(1955, 5, 12))
    for (persoana in agenda){
        persoana.Print()
    }
    println("Agenda dupa eliminare contact [George]:")
    agenda.removeAt(1)
    for (persoana in agenda){
        persoana.Print()
    }
    Cautare(agenda)

    agenda.remove(Contact("Liviu" , "0231450211", Birth(1999, 7, 30)))
    println("Agenda dupa eliminare contact [Liviu]:")
    agenda.removeAt(1)
    for (persoana in agenda){
        persoana.Print()
    }
}