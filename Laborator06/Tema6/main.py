import os

class GenericFile:
    def __init__(self, path_absolut, frecvente):
        if not path_absolut and not frecvente:
            raise NotImplementedError("Eroare")
        self.path_absolut = path_absolut
        self.frecvente = frecvente

    def get_path(self):
        pass

    def get_freq(self):
        pass

class TextASCII(GenericFile):
    path_absolut = ""
    frecvente = []

    def __init__(self, path_absolut, frecvente):
        self.path_absolut = path_absolut
        self.frecvente = frecvente

    def get_path(self) -> str:
        return self.path_absolut

    def get_freq(self)-> int:
        return self.frecvente

    def __repr__(self):
        return self.path_absolut

class TextUNICODE(GenericFile):
    path_absolut = ""
    frecvente = []

    def __init__(self, path_absolut,frecvente):
        self.path_absolut = path_absolut
        self.frecvente = frecvente

    def get_path(self) -> str:
        return self.path_absolut

    def get_freq(self) -> int:
        return self.frecvente

    def __repr__(self):
        return self.path_absolut

class Binary(GenericFile):
    path_absolut = ""
    frecvente = []

    def __init__(self, path_absolut, frecvente):
        self.path_absolut = path_absolut
        self.frecvente = frecvente

    def get_path(self) -> str:
        return self.path_absolut

    def get_freq(self) -> int:
        return self.frecvente

    def __repr__(self):
        return self.path_absolut

def Frequency(path_absolut) -> []:
    freq = []
    for i in range(255):
        freq.append(0)

    file = open(path_absolut,'r')
    for line in file:
        for character in line:
            freq[ord(character)] = freq[ord(character)] + 1

    return freq

def Tip(freq) -> str :
    sumUNICODE = 0
    sumASCII = 0

    for i in range(255):
        sumUNICODE = sumUNICODE + freq[i]

    freq_0 = freq[ord('0')]
    if 0.3 * sumUNICODE <= freq_0:
        return "UNICODE"
    else:
        return "ASCII"

if __name__ == '__main__':
    binar = []
    ascii = []
    unicode = []
    filename = os.listdir(os.getcwd())
    for file in filename:
        if file.endswith(".txt"):
            vector = Frequency(file)
            rez = Tip(vector)
   #for i in range(len(vector)):
       #print(i," ",vector[i],)
            if rez == "ASCII":
                ascii.append(TextASCII(file, vector))
            elif rez == "UNICODE":
                unicode.append(TextUNICODE(file, vector))

    print("TEXT ASCII: ", ascii)
    print("TEXT UNICODE:", unicode)