import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

fun Int.IsPrim():Boolean {
    for (i in 2.. this/2) {
        if (this % i == 0) {
            return false
        }
    }
    return true
}


fun main() {
    println("Aplicatia 1 :")
    var x = 17
    println(x.IsPrim())
    var y = 20
    println(y.IsPrim())

    println("Aplicatia 2 :")
    val str1 = "May 18, 2021"
    val formatter = DateTimeFormatter.ofPattern("MMMM d, yyyy", Locale.ENGLISH)
    val date = LocalDate.parse(str1,formatter)
    println(date)
    
}