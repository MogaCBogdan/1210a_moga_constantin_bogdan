//ex1
val capitalize = { str: String -> str.capitalize() }
//ex2
fun <T>varargFun(vararg items: T) {
    items.forEach(::println)
}
//ex3
fun <T> emit(t: T, vararg listeners: (T) -> Unit) = listeners.forEach{
    listener -> listener(t)
}
//ex4
fun <T> transform(t: T, fn: (T) -> T): T {
    return fn(t)
}
//ex5 functie clasica ca parametru
fun reverse(str: String): String {
    return str.reversed()
}
//ex6 functii dintr-o clasa ca parametru in alte functii
class Transformer {
    fun upperCased(str: String): String {
        return str.toUpperCase()
    }
    companion object {
        fun lowerCased(str: String): String {
            return str.toLowerCase()
        }
    }
}
//ex7
fun performOperationOnEven(number:Int,operation:(Int)->Int):Int {
    if (number% 2 == 0) {
        return operation(number)
    } else {
        return number
    }
}




fun main() {
    //ex1
    println("---- Exemplul 1 -----")
    println(capitalize("hello world!")) //litera mare primul caracter

    //ex2
    println("---- Exemplul 2 -----")
    varargFun(1)
    varargFun (1.1,2.2)
    varargFun("Cristina","Andrei","Roxana")

    //ex3
    println("---- Exemplul 3 -----")
    emit(1, ::println, {i -> println(i * 2)})
    emit(
        listOf('a', 'b', 'c'),
        ::println,
        {list -> println(list.joinToString(prefix="<",
            postfix=">",
            separator=""))}
    )
    //ex4
    println("---- Exemplul 4 -----")
    println(transform("kotlin", { str: String -> str.capitalize() }))
    println(transform("kotlin", { it.capitalize() })) //it = parametru implicit pentru functiile lambda cu un parametru

    //ex5
    println("---- Exemplul 5 -----")
    println(transform("kotlin", ::reverse))

    //ex6
    println("---- Exemplul 6 -----")
    val transformer = Transformer()
    println (transform("kotlin", transformer::upperCased))
    println (transform("kotlin", Transformer.Companion::lowerCased))

    //ex7
    println("---- Exemplul 7 -----")
    println("Called with 4,(it*2):${performOperationOnEven(4, {it* 2 })}")
    println("Called with 5,(it*2):${performOperationOnEven(5, {it* 2 })}")


}
