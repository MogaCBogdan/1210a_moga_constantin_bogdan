class Book(private val data : Content){
    override fun toString(): String {
        return "Autor: " + data.author + ", Titlu: " + data.name + ", Editura: " + data.publisher + "\nContinut: " + data.text + "\n"
    }

    fun GetName() :String? {
        return data.name
    }

    fun GetAuthor() : String? {
        return data.author
    }

    fun GetPublisher() : String?{
        return data.publisher
    }

    fun GetContent() : String? {
        return data.text
    }

    fun HasAuthor(name : String) : Boolean{
        return data.author.equals(name)
    }

    fun HasTitle(name : String) : Boolean {
        return data.name.equals(name)
    }

    fun PublishedBy(name : String) : Boolean {
        return data.publisher.equals(name)
    }
}
