class Library{
    private val Books = mutableSetOf<Book>()
    fun GetBooks() : Set<Book> {return Books}

    fun AddBook(B : Book){
        Books += B
    }

    fun FindAllByAuthor(author : String) : Set<Book> {
        val result = mutableSetOf<Book>()
        for (b in Books){
            if (b.HasAuthor(author))
                result += b
        }
        return result
    }

    fun FindAllByName(title : String) : Set<Book> {
        val result = mutableSetOf<Book>()
        for (b in Books){
            if (b.HasTitle(title))
                result += b
        }
        return result
    }

    fun FindAllOfPublisher(publisher : String) : Set<Book> {
        val result = mutableSetOf<Book>()
        for (b in Books){
            if (b.PublishedBy(publisher))
                result += b
        }
        return result
    }
}