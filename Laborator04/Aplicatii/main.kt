fun main(args : Array<String>){
    val my_books = arrayOf<Book>(
        Book(Content("EMIL DINGA","De ce tristete, nu e greu de spus. Nu numai corsetul timpului, dar si corsetul viselor noastre despre lumea in care am fost heideggerian aruncati ne constrang mai degraba","Voluptatea tristetii. Poezii - Emil Dinga","Curtea Veche Publishing")),
        Book(Content("STEPHENIE MEYER","Viata adolescentei Bella Swan capata o turnura diferita din momentul in care se muta in alt orasel, pentru a locui cu tatal ei. Initial totul i se pare deprimant, pentru ca trebuie sa se acomodeze","Amurg. Oricat de perfecta ar fi o zi, intotdeauna se termina","Paladin")),
        Book(Content("STEPHEN HAWKING","De unde vine universul? Cum si cand a inceput? Va ajunge la un sfarsit, si daca da, cum? Acestea sunt intrebari care ne intereseaza pe toti","Scurta istorie a timpului. De la Big Bang la gaurile negre","Humanitas")),
        Book(Content("STEPHEN HAWKING","Universul intr-o coaja de nuca, publicata pentru prima data in Anglia si Statele Unite in 2001, continua celebrul best-seller Scurta istorie a timpului si se adreseaza publicului larg","Universul intr-o coaja de nuca","Humanitas")),
        Book(Content("STEPHEN HAWKING","Se spune ca realitatea este uneori mai ciudata decat fictiunea, ceea ce nicaieri nu este mai adevarat decat in cazul gaurilor negre","Gaurile negre. Prelegerile Reith","Humanitas")) ,
        Book(Content("STEPHEN HAWKING","De ce ne aflam aici? De ce exista ceva mai degraba decat nimic? Care este natura realitatii? De ce legile naturii sunt atat de fin reglate","Marele plan","Humanitas"))
    )

    val lib = Library()
    my_books.forEach {
        lib.AddBook(it)
    }

    val Serializer = LibraryPrinter(lib)
    Serializer.PrintBooksRaw()
    Serializer.PrintHTML()
    Serializer.PrintJSON()
}
