import java.io.File

class LibraryPrinter(private val lib : Library){
    private val available_books = lib.GetBooks()

    fun PrintBooksRaw(){
        available_books.forEach {
            println("$it")
        }
    }

    fun PrintHTML(){
        val content = File("Librarie.html").writer()
        content.write("<html>\n")
        content.write("<head><title>LibrarieL</title></head>\n")
        content.write("<body>")
        available_books.forEach {
            content.write("<p><h2>")
            content.write("$it")
            content.write("</h2></p>")
        }
        content.write("</body>")
        content.write("</html>")
        content.close()
    }

    fun PrintJSON(){
        val content = File("Librarie.json").writer()
        content.write("[\n")
        available_books.forEach {
            if (it != available_books.last())
                content.write("    {\"Titlu\": \"${it.GetName()}\", \"Autor\":\"${it.GetAuthor()}\", \"Editura\":\"${it.GetPublisher()}\", \"Text\":\"${it.GetContent()}\"},\n")
            else
                content.write("    {\"Titlu\": \"${it.GetName()}\", \"Autor\":\"${it.GetAuthor()}\", \"Editura\":\"${it.GetPublisher()}\", \"Text\":\"${it.GetContent()}\"}\n")
        }
        content.write("]\n")
        content.close()
    }
}