//import libraria principala polyglot din graalvm
import org.graalvm.polyglot.*;

//clasa principala - aplicatie JAVA
class Polyglot {
    //metoda privata pentru conversie low-case -> up-case folosind functia toupper() din R
    private static String RToUpper(String token){
        //construim un context care ne permite sa folosim elemente din R
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        Value result = polyglot.eval("R", "toupper(\""+token+"\");");
        //utilizam metoda asString() din variabila incarcata cu output-ul executiei pentru a mapa valoarea generica la un String
        String resultString = result.asString();
        polyglot.close();

        return resultString;
    }

    //metoda privata pentru evaluarea unei sume de control simple a literelor unui text ASCII, folosind PYTHON
    private static int SumCRC(String token){
        Context polyglot = Context.newBuilder().allowAllAccess(true).build();
        Value result = polyglot.eval("python", "sum(ord(ch) for ch in '" + token + "[1:-1]')"); //Elimina primul si ultimul caracter din suma
        //utilizam metoda asInt() din variabila incarcata cu output-ul executiei, pentru a mapa valoarea generica la un Int
        int resultInt = result.asInt();
        polyglot.close();

        return resultInt;
    }

    //functia MAIN
    public static void main(String[] args) {
        Context polyglot = Context.create();
        //construim un array de string-uri, folosind cuvinte din pagina web:  https://chrisseaton.com/truffleruby/tenthings/
        Value array = polyglot.eval("js", "[\"If\",\"we\",\"run\",\"the\",\"java\",\"nur\",\"ew\",\"included\",\"in\",\"rather\",\"than\",\"setting\",\"up\",\"a\",\"micro\",\"and\",\"I\",\"will\",\"use\",\"a\",\"large\",\"input\",\"so\",\"that\",\"we\",\"arent\",\"GraalVM\"];");
        int[] sumascii = new int[(int) array.getArraySize()];
        String[] Words = new String[(int)array.getArraySize()];

        for (int i = 0; i < array.getArraySize();i++){
            String element = array.getArrayElement(i).asString();
            String upper = RToUpper(element);
            int crc = SumCRC(upper);
            sumascii[i] = crc;
            Words[i] = upper;
        }

        int auxnr;
        String auxnume;

        //Sortez in ordine crescatoare
        for(int i = 0; i < array.getArraySize()-1 ; ++i){

            for(int j = i+1; j < array.getArraySize(); ++j){

                if(sumascii[i] > sumascii[j]){

                    auxnr = sumascii[i];
                    sumascii[i] = sumascii[j];
                    sumascii[j] = auxnr;

                    auxnume = Words[i];
                    Words[i] = Words[j];
                    Words[j] = auxnume;

                }
            }
        }

        for(int i = 0; i <array.getArraySize()-1; ++i){

            System.out.print("Codul ascii : " + sumascii[i] + " ");

            while(sumascii[i] == sumascii[i+1]){
                System.out.print("- " + Words[i] + " ");
                System.out.print('\n');
                i++;
            }

            System.out.print(" ->" + Words[i]);
            System.out.print('\n');
        }

        if(sumascii[(int)array.getArraySize()-1] == sumascii[(int) array.getArraySize()-2]){
            System.out.print(Words[(int)array.getArraySize()-1]);
        }
        else{
            System.out.print("Cod ascii: " + sumascii[(int)array.getArraySize()-1] + " -> " + Words[(int)array.getArraySize()-1]);
        }

        polyglot.close();
    }
}